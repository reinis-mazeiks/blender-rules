import praw

from bot.comment_response import CommentResponse, get_first_actionable_comment_response
from bot.post_response import PostResponse, get_first_actionable_post_response
from community.community import Community
from rules_comment.constructive_criticism import get_constructive_criticism_response
from rules_comment.good_bot import get_good_bot_response
from rules_comment.mark_solved import get_mark_solved_response
from rules_comment.swearing import get_swearing_response
from rules_post.caps_title import get_caps_title_response
from rules_post.faq import get_faq_response


class BlenderHelp(Community):
    def get_submission_response(self, submission: praw.models.Submission) -> PostResponse:
        serious_response = get_first_actionable_post_response([
            get_faq_response(submission)
        ])

        # easter eggs and less serious responses
        # (only if none of the "priority" helpful responses were triggered)
        response = get_first_actionable_post_response([
            serious_response,
            get_caps_title_response(submission)
        ])

        return response

    def get_comment_response(self, comment: praw.models.Comment) -> CommentResponse:
        swearing_response = get_swearing_response(comment)

        if not swearing_response.is_nothing():
            return swearing_response

        return get_first_actionable_comment_response([
            get_mark_solved_response(comment),
            get_constructive_criticism_response(comment),
            get_good_bot_response(comment),
        ])
