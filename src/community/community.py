import abc

import praw

from bot.comment_response import CommentResponse
from bot.post_response import PostResponse


class Community(abc.ABC):
    @abc.abstractmethod
    def get_submission_response(self, submission: praw.models.Submission) -> PostResponse:
        pass

    @abc.abstractmethod
    def get_comment_response(self, submission: praw.models.Comment) -> CommentResponse:
        pass
