import praw

from bot.comment_response import CommentResponse, get_first_actionable_comment_response
from bot.post_response import PostResponse, merge_post_responses, get_first_actionable_post_response
from community.community import Community
from rules_comment.constructive_criticism import get_constructive_criticism_response
from rules_comment.good_bot import get_good_bot_response
from rules_comment.swearing import get_swearing_response
from rules_post.caps_title import get_caps_title_response
from rules_post.excessive_promotion import get_excessive_promotion_response
from rules_post.help_questions import get_missing_help_flair_response
from rules_post.missing_critique_flair import get_missing_critique_flair_response
from rules_post.roast_warning import get_roast_warning_response
from rules_post.robot_artwork import get_robot_artwork_response


class Blender(Community):
    def get_submission_response(self, submission: praw.models.Submission) -> PostResponse:
        serious_response = merge_post_responses([
            get_roast_warning_response(submission),
            get_excessive_promotion_response(submission),
            get_first_actionable_post_response([
                get_missing_help_flair_response(submission),
                get_missing_critique_flair_response(submission),
            ])
        ])

        # easter eggs and less serious responses
        # (only if none of the "priority" helpful responses were triggered)
        response = get_first_actionable_post_response([
            serious_response,
            get_caps_title_response(submission),
            get_robot_artwork_response(submission),
        ])

        return response

    def get_comment_response(self, comment: praw.models.Comment) -> CommentResponse:
        swearing_response = get_swearing_response(comment)

        if not swearing_response.is_nothing():
            return swearing_response

        return get_first_actionable_comment_response([
            get_constructive_criticism_response(comment),
            get_good_bot_response(comment),
        ])
