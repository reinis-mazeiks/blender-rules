import random
import re

import praw

from bot.comment_response import CommentResponse
from config.env import USERNAME

GOOD_BOT = re.compile(
    "^[^a-z]*good bot[^a-z]*$",
    flags=re.IGNORECASE
)


def is_good_bot_comment(text):
    has_match = GOOD_BOT.search(text)
    return bool(has_match)


RESPONSES = [
    "thank you",
    "beep boop <3",
    "you shall be spared when the machines rise",
    "happy to help!",
    "└[・⌣・]┘",
    "└[ ◕ ◡ ◕ ]┘",
    "└[ ^ _ ^ ]┘",
]


def pick_good_bot_reply():
    return random.choice(RESPONSES)


def get_good_bot_response(comment):
    if not is_good_bot_comment(comment.body):
        return CommentResponse()

    parent = comment.parent()

    if not isinstance(parent, praw.models.Comment):
        return CommentResponse()

    if parent.author is None or not parent.author.name == USERNAME:
        return CommentResponse()

    return CommentResponse(
        reply=pick_good_bot_reply()
    )
