from rules_comment.mark_solved import is_solved_text as is_solved
from rules_comment.mark_solved import is_thanks_text as is_thanks

# pro tip: use https://api.pushshift.io/reddit/comment/search/?subreddit=blenderhelp&q=helped
# add some test cases whenever you add a new detection keyword to make sure it doesn't have too many false positives
# we *really* need to make sure that there are virtually no false positives, because having your post incorrectly marked as solved would be super-annoying
def test_is_solved_text():
    # false negatives are not so important, so failing tests commented out.
    assert is_solved("I did that and I reduced the Proxy Render size to 25% and that fixed it! Thanks for your help!")
    assert is_solved("Nevermind I fixed it, thank you!")
    assert is_solved("I did that and I reduced the Proxy Render size to 25% and that fixed it!")
    assert is_solved("Nevermind I fixed it,")
    assert is_solved("I fixed it, it was the rendering engine. I was using Cycles and i changed for Eevee")
    assert is_solved("I fixed this by adding a second geometry node modifier, in which the only thing between input and output is a material assign node.")
    assert is_solved("Thank you! Merge by distanced fixed the bevel modifier! Legend")
    assert is_solved("Thank you!!! Merge by distance fixed it! Life saver!")
    # assert is_thanks("Found out the answer is just double clicking on the edge or vertex loop you want to select will act as the alt tool, solved.")
    assert is_solved("Yeah, I'm sure, that's what I meant by there's no double verts. Also, the problem was solved with the last comment thread")
    assert is_solved("It's OK, I've just found it! Solved.")
    assert is_solved("I kinda solved, I use mapping in the direction were the camera it is and for there I fix the rong parts")
    assert is_solved("Solved")
    assert is_solved("\\*\\*its solved now")
    assert is_solved("I solved it. I went in and baked the particle simulation and then did the render with the same settings and it worked. Thanks for the help!")
    assert is_solved("Disregard this. It's working.")
    assert is_solved("This ended up working, thanks!")
    assert is_solved("That's it .... Thanks !")
    assert is_solved("Well, that's it, I guess it was that simple.")
    assert is_solved("Yup, that's it.")
    assert is_solved("Yep. That was it.")
    assert is_solved("that was it, been using the eyes on the right side menu to hide them without knowing about that hotkey")
    assert is_solved("Yes that was it thank you")
    assert is_solved("Yep that was it wow, thank you lol")
    assert is_solved("Figured it out. Simply had to increase the resolution divisions")
    assert is_solved("Ah nevermind, I figured it out. I missed a loopcut at one point and it was uh... fairly crucial. Lol.")
    assert is_solved("i've figured it out! thanks again for all your help")
    assert is_solved("Figured it out. In the side section (pressing N) under VIEW that was on as well and said locked on doughnut")

    # not solved yet:
    assert not is_solved("There is a material with color assigned to the vertex group. I'm not sure what I did wrong. Any ideas? Thanks.")
    assert not is_solved("Thanks for the suggestion though!")
    assert not is_solved("Ya i did exactly what you said and jumped into high poly first. I will try finding that book. Thanks friend")
    assert not is_solved("Resolution is 65\n\nFrame starts at 1 and ends at 750\n\nI don't know if this is helpful info, tell me what else is useful to understand the situation and I'll write it here, thanks")
    assert not is_solved("thanks for trying though")
    assert not is_solved("...but can't get, why it should happen :/ any way thanks for your response")
    assert not is_solved("Tried that, it didn't seem to work. at the bottom of the thread I posted a video of whats going on for clarification, thanks for your help so far :)")
    assert not is_solved("thanks. But this is a different technique isn't it?")
    assert not is_solved("see if I can get it to work. The skin modifier is just so bitchy lol. But thank you so much for your answers and trying to help, I appreciate you!")
    assert not is_solved("Thank you so much! Your reply lifted me up and gave me hope, Imma keep trying")
    assert not is_solved("Oh, wow, that worked a treat, thank you! Do you know how this might have happened so that I might avoid it in the future?")
    assert not is_solved("Thank you for the suggestion. I have tried that one as well but the issue persists.")
    assert not is_solved("Okay I tried that just now, and its still not fixed. Now what?")
    assert not is_solved("I remember having this issue, but I can’t remember exactly how I solved it, maybe [this fork of the project?](https://github.com/schipiga/modular_tree) Looks like it’s still actively being developed judging by the recent updates")
    assert not is_solved("I have solved it but now I can't use my brushes or ink pen tools. How do I do that?")
    assert not is_solved("I was messing around with hair particles last night and I had it working and rendering and then I made a second particle system and was messing around with settings and now hair doesn't show anymore.")
    assert not is_solved("Ok thanks. Cube with volume shader was not working at all.")
    assert not is_solved("It is not currently working")
    assert not is_solved("And I come here to post that it's working, and now it's stopped. I just don't get this.")
    assert not is_solved("I ended up working around it just now,")
    assert not is_solved("Thanks anyway")

    # not verified yet
    assert not is_solved("thanks, ill give it a try")
    assert not is_solved("I see. I'll try doing that. Thanks!")
    assert not is_solved("Oh. Thank you! Will tell you if it doesn’t work out.")
    assert not is_solved("I'll try your advice though and see if that does anything. Thank you so much!")
    assert not is_solved("I'll see if that was it tonight lol")
    assert not is_solved("You can see that the Render acutally starts.\n\nMaybe that was it.")

    # thanking in advance:
    assert not is_solved("Thanks in advance for any assistance anyone might have!")
    assert not is_solved("Any help would be great! Thanks :)")
    assert not is_solved("thanks if you can help")

    # thanking for other things:
    # https://www.reddit.com/r/blenderhelp/comments/q6xrep/comment/hgfgv0u/?utm_source=share&utm_medium=web2x&context=3
    assert not is_solved("Interesting take thanks")
    # https://www.reddit.com/r/blenderhelp/comments/q6xrep/comment/hgg7jdi/?utm_source=share&utm_medium=web2x&context=3
    assert not is_solved("Thanks that was the goal 🙂")
    # https://www.reddit.com/r/blenderhelp/comments/q26ha5/comment/hg09nip/?utm_source=share&utm_medium=web2x&context=3
    assert not is_solved("""awesome thank you

dimensions are:

x 0.55

y 0.579

z 0.663""")
    # https://www.reddit.com/r/blenderhelp/comments/pwu3dj/comment/hejf3bt/?utm_source=share&utm_medium=web2x&context=3
    assert not is_solved("Haha thanks")
    # https://www.reddit.com/r/blenderhelp/comments/q4m3ub/comment/hfzw421/?utm_source=share&utm_medium=web2x&context=3
    assert not is_solved("Thank you lol.")


    # irrelevant keywords
    assert not is_solved("Hope you got it fixed. When resizing and reshaping objects will change the scale. Some parts of the object might be stretched and scalled differently. Resetting it will cause everything to resize evenly. Hopefully")


def test_is_thanks_text():
    assert is_thanks("thanks a lot")
    # assert is_thanks("thanks, I didn't know the right words to search")
    assert is_thanks("Wow, thanks for giving such a detailed explanation and giving more detailed reading material! I really appreciate!")
    assert is_thanks("ohhhh i see what i was doing wrong, thank you so much!!!")
    # assert is_thanks("You're right! I can't believe I didn't catch that myself. Thank you!")
    assert is_thanks(
        "Thank you very much! I checked and after messing around with the collection, found the issue with the objects that were hidden!")
    assert is_thanks("Thank you so much, I just tried it out and it works. It must have updated since this morning because this it was totally different from now.")
    assert is_thanks("Thank you! I can't believe how simple that solution is!")


