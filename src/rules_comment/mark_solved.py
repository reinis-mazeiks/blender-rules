import re

from bot.comment_response import CommentResponse
from bot.post_response import PostResponse
from config.blender_flairs import UNSOLVED_MAP, SOLVED_MAP

SOLVED_KEYWORDS = re.compile(
    f"\\b(fixed|solved|working)\\b|"
    f"\\b(that'?s it|that was it|figured it out)\\b",
    flags=re.IGNORECASE
)

THANKS_KEYWORDS = re.compile(
    f"\\b(thanks|thank you)(?! (anyway|though|tho|nevertheless|for the reply))\\b",
    flags=re.IGNORECASE
)

# for filtering out things like "thanks, but it's still not working"
EXCLUDE_KEYWORDS = re.compile(
    f"\\b(but|however|still|yet|though|tho|although|nevertheless)\\b|"  # still has an issue
    f"\\b(didn'?t|doesn'?t|isn'?t|wasn'?t|hasn'?t|hadn'?t|don'?t)\\b|"
    f"\\b(work ?around|working around|temporary|hack)\\b|"
    f"\\b(pain|bad|inefficient|ugly|suboptimal|hard)\\b|"
    f"\\b(nope|not)\\b|"
    f"\\b(in advance|please|pls)\\b|"  # thanking in advance
    f"\\b(try|trying|maybe|i'll|will|won't|would|if)\\b|"  # perhaps the solution still hasn't been verified yet
    f"(:\\|)|(:\\()|(:/)|(:\\\\)|"  # :| and :( and :/ and :\ but its ugly bc regex
    f"\\?",  # still contains a question
    flags=re.IGNORECASE
)

NOTICE_UPDATED = "Sounds to me like the issue is solved, so I've flaired it as such. If I made a mistake, I'm sorry – feel free to change it back!"
SUGGESTION_UPDATE = """
If your issue is solved, please update the flair!

You can also reply "solved" to this comment :)
"""


def is_solved_text(text):
    # long text likely to have more false positives
    # we really care about false positives
    # nlp is hard.
    if len(text) > 150:
        return False

    if not bool(SOLVED_KEYWORDS.search(text)):
        return False

    return not bool(EXCLUDE_KEYWORDS.search(text))


def is_thanks_text(text):
    if len(text) > 150:
        return False

    if not bool(THANKS_KEYWORDS.search(text)):
        return False

    return not bool(EXCLUDE_KEYWORDS.search(text))


def get_mark_solved_response(comment):
    flair = comment.submission.link_flair_template_id if hasattr(comment.submission, "link_flair_template_id") else None

    if not comment.is_submitter:
        return CommentResponse()

    subreddit = comment.subreddit.display_name
    unsolved_flair = UNSOLVED_MAP.get(subreddit)
    solved_flair = SOLVED_MAP.get(subreddit)

    if not flair == unsolved_flair:
        return CommentResponse()

    if comment.is_submitter:
        if is_solved_text(comment.body):
            return CommentResponse(
                reply=NOTICE_UPDATED,
                parent_submission_response=PostResponse(
                    set_flair=solved_flair
                )
            )

        if is_thanks_text(comment.body):
            return CommentResponse(
                reply=SUGGESTION_UPDATE,
            )

    return CommentResponse()
