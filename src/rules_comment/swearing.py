import re
from pathlib import Path

from bot.comment_response import CommentResponse
from language_util.bot_language import join_keywords


def read_blacklist():
    path = Path(__file__).parent / "./swearing_wordlist.txt"

    with open(path) as f:
        return re.compile(
            "|".join(f"\\b{entry.strip()}\\b"
                     for entry in f
                     if len(entry) > 0
                     ),
            flags=re.IGNORECASE
        )


BLACKLIST = read_blacklist()

symbol = "[^\\s\\w]"
fuck = f"f+(u|{symbol})*[ck]{{0,5}}[ck]"

OBSCENITIES = re.compile(
    f"{fuck}([a4]+|[e3r]+r*)\\b|"
    f"\\b(m(o+|0+|u+)(d+|th)[ae43]r?[^\\w]{{0,2}}(f+[uck]{{0,5}}[ck]([a4]+|[e3]+r+|[i1]*n+g*)))\\b|"
    f"\\b(piss|{fuck}) (off)\\b|"
    f"\\bb(i|{symbol})+t+ch\\b|"
    f"\\b(shut the {fuck})"
)


def find_obscenities(text: str):
    regex_results = [
        violation.group()
        for violation in OBSCENITIES.finditer(text)
    ]

    blacklist_results = [
        violation.group()
        for violation in BLACKLIST.finditer(text)
    ]

    return {*regex_results, *blacklist_results}


def get_swearing_response(comment):
    obscenities = find_obscenities(comment.body)

    if len(obscenities) > 0:
        return CommentResponse(report=f"Rude? {join_keywords(obscenities)}")

    return CommentResponse()
