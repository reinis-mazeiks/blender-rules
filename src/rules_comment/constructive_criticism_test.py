from rules_comment.constructive_criticism import find_mean_words

expected_mean_words = {
    "It's terrible, it looks shit.": {"it looks shit"},
    "Looks shit tbh. No creativity, just trash": {"Looks shit"},
    "tell you what, you have no fucking talent": {"you have no fucking talent"},
    "lmao you're a total noob haha": {"you're a total noob"},
    "just came here to say you're bad, gtfo": {"you're bad"},
    "it's garbage.": {"it's garbage"},

    "I don’t think it looks terrible at all! It’s definitely got potential for sure.": set(),
    "Holy shit dude great job! Looks so damn real. Could you tell me how you made the plants and lily pads and stuff? Is that from an add on or self modeled?": set(),
    "Nice! I tried to make one with a texture mapping trick, but it looked horrible, so I can appreciate this! It looks like you made each letter as part of the model?": set(),
    "I've noticed this guy is pretty much following you around reddit and shitting on you every time you post. Make sure to use the following link to report him to the reddit admins for harassment": set(),
}


def test_find_mean_words():
    for text, expected in expected_mean_words.items():
        assert find_mean_words(text) == expected
