from rules_post.missing_critique_flair import find_critique_keywords

expected_critique_keywords = {
    "wine glasses on a table (realism attempt--critique welcome)": ["critique"],
    "My attempt to make a tesseract..let me know how I can improve on it": ["how I can improve"],
    "This is my attempt to set up a living room in Blender, what do you think? (Modells are Downloaded Btw)": [
        "what do you think"],
    "First time doing product render criticism welcome": ["criticism welcome"],
    "My first render. What do you think? Any suggestions?": ["What do you think", "suggestions?"],
    "Cafe update 3: how about now? What can i improve on? Feedback? (Last wip post)": ["What can i improve",
                                                                                       "Feedback"],
    "How can I improve?": ["How can I improve"],
    "I think my fifth? render. Feedback would be appreciated": ["Feedback"],
    "Made with Grant Abbitt’s Well tutorial. This is sort of up for critique but please don’t be too harsh, this is my first real project. From here I plan to make a low poly sailing boat without a tutorial and take it from there! If the boat is a success a will post a photo link in the comments!": [
        "critique"],
    "First ever render. A birthday present for my friend. Any feedback will be appreciated.": [
        "feedback"],
    "What's missing to make this city look more realistic?": ["What's missing to make this city look more realistic?"],
    "I am very new to blender and this is one of my first real projects. Can I have some tips?": [
        "Can I have some tips"],
    "I watched Inside by Bo Burnham last week and it's so good that I decided to do this recreation of the room in blender. Do you guys have any feedback?": [
        "feedback"],
    "My injured friend doesn't look good on paper... Workflow of a guy who got injured > Blender + Unity. Any thoughts?": [
        "thoughts?"],
    "I made my first small environment render in cycles...needs suggestions on improving it further...": [
        "needs suggestions"],
    "Got inspired by myinputwo on instagram! Critiques welcomed!": ["Critiques"],
    "Critique and Advice Away! Looking for Constructive Advice. Thank you.": ["Advice Away",
                                                                              "Constructive Advice", "Critique"],
    "My First Project: My own pc setup (tips appreciated)": ["tips appreciated"],
    "Neon Sign Simulation - Feedback and Thoughts?": ["Feedback", "Thoughts?"],
    "'Another one for the audience' (suggestions for improvement appreciated)": [
        "suggestions for improvement appreciated"],
    "Can somebody tell me tips how can I improve her so she could look better than she is now?": ["tips how can I improve her so she could look better than she is now?"],
    "A interior design render.....It's still a work in progress. Any suggestions to what i should add?": [
        "suggestions to what i should add?"],
    "does someone have any tips or ideas for creating realistic minecraft blocks and fluids? Sort of like Jakefellman (his creations are below)": [
        "tips or ideas for creating realistic minecraft blocks and fluids?"],
    "Two Great Swords that I made, feedback welcome": ["feedback"],
    "Appreciate any tips on improving these orthographic view type art!": ["Appreciate any tips"],
}


def test_find_help_keywords():
    for title, expected_keywords in expected_critique_keywords.items():
        assert sorted(find_critique_keywords(title)) == sorted(expected_keywords)
