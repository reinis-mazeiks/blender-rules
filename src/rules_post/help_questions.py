import re

from bot.post_response import PostResponse
from config import blender_flairs as flairs
from language_util.bot_language import join_keywords
from language_util.common_regex import somebody_keywords, could_keywords, please_keywords

HELP_KEYWORDS = re.compile(
    # stuff won't work
    f"\\b(doesn'?t|won'?t) (show up|work|render)\\b|"
    f"\\b(isn'?t|aren'?t) (showing up|working|rendering)\\b|"
    f"\\bi (can'?t|couldn'?t)\\b|"

    # help pls
    f"\\bnot sure if this has been asked\\b|"
    f"\\b(does|do|would) {somebody_keywords} know\\b|"
    f"\\b(please|need) (help|advise|advice)\\b|"
    f"\\bhelp (\\w+ ){{0,2}}{please_keywords}\\b|"
    f"\\bhelp me\\b|"
    f"\\b{could_keywords} {somebody_keywords} (\\w+ ){{0,3}}help\\b|"
    f"^help\\b|"

    # problems
    f"(^|i have |i'?m having )(\\w+ ){{0,2}}(issues?|problems?|errors?)\\b|"
    f"\\b(issues?|problems?|errors?)($| appeared| started)|"
    f"\\berrors?\\b|"

    # common questions
    f"\\bwhat is going on\\b|"
    f"\\b(is there a way|is it possible to)\\b|"
    f"\\bhow (do you|do i|can you|can i)\\b|"
    f"\\bhow to[^.!?:]*\\?|"

    # misc
    f"\\b(i have no clue|i don'?t know)\\b",
    flags=re.IGNORECASE
)


def find_help_keywords(text):
    return [
        match.group()
        for match in HELP_KEYWORDS.finditer(text)
    ]


def construct_advice(keywords):
    joined_keywords = join_keywords(keywords)

    return f"""
I noticed {joined_keywords} in your post.

If you're **asking for help** on a specific problem, the best place to post would be **r/blenderhelp** – lots of helpers check there and you would be more likely to get an answer!

(If you're **asking for feedback** on your work, you're in the right place – but consider adding the **"Critique" flair**)
"""


# flairs that the user might incorrectly add when asking for feedback
WRONG_FLAIRS = {
    flairs.I_MADE_THIS
}


def get_missing_help_flair_response(submission):
    flair = submission.link_flair_template_id if hasattr(submission, "link_flair_template_id") else None

    if flair not in WRONG_FLAIRS:
        return PostResponse()

    keywords = [
        *find_help_keywords(submission.title),
        *find_help_keywords(submission.selftext),
    ]

    if len(keywords) == 0:
        return PostResponse()

    return PostResponse(
        reply=construct_advice(keywords)
    )
