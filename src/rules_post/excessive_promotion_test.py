from .excessive_promotion import find_promo_keywords

expected_keywords = {
    "I made Gol D. Roger's ship Oro jackson. Anime: OnePiece My Instagram account": {"My Instagram"},
    "I made Gol D. Roger's ship Oro jackson. twitch account:hubble_3d": {"twitch account:"},
    "A futuristic ship scene i had made a while ago 😁 ps~ @kshitijsart is my instagram handle hehe": {"my instagram handle"},
    "Heaven {22.05.2021}. My latest artwork made using blender. Check out my instagram for more of my work.": {"my instagram"},
    "Pocket Watch, The first artwork in the Super Realistified series on my Instagram - @carrot.boiii": {"my Instagram"},
    "I made this retro style animation loop for a client's song!!! How is it?? Yall can check out my Instagram @siddharthxotwod and YouTube:https://youtu.be/-9Y4T99rlqQ": {"my Instagram", "YouTube:", "https://youtu.be/-9Y4T99rlqQ"},
    "Hi ! A few month back, I decided to launch a 52 weeks challenge on my Instagram (@arnaudcroonen) where I model and render something every week for a year. It is now week 30 already ! If you want to follow my work, it would help me so much, thanks a lot ❤️": {"my Instagram"},
    "I'm currently telling a story through images and text on my Instagram (@vincentkeep) ! This is my first time really working in 3D so would love to hear people's thoughts!": {"my Instagram"},
    "After 10 years being a 3d artist, finally opened Blender with Octane render... I can say this is probably the most powerful 3d software! P.S.: The first hour in blender. Follow me to watch the progress ;)": {"Follow me"},
    "Joe Rogan Experience Studio render made in Blender. Since Lockdowns I have started listening to a lot of podcasts and JRE is my absolute favourite. and I really like the feel of new studio so I recreated it in 3d, Please share on instagram with Joe Rogan https://www.instagram.com/reel/CRhBrXBnmcm/" : {"Please share", "https://www.instagram.com/reel/CRhBrXBnmcm/"},
    "My blender Low Poly animation channel hits 35 subscribers, And I am so Happy!!!!" : {"35 subscribers", "My blender Low Poly animation channel"},
    "Katana Animation - Insta sobeswag_art" : {"Insta sobeswag_art"},
    "Follow me on Insta @tripppytaurus" : {"Follow me", "Insta @tripppytaurus"},
    "Stylized render of music artist Charli XCX, Similar work on my insta @DigitalBurial" : {"my insta"},
    "I made a CoD:MW inspired teaser trailer YT:IconigonArt" : {"YT:"},
    "What do you guys think about my crispy’Kriss animation? Feel free to give me an advice. YT:kranosky" : {"YT:"},
    "I made a smol Trese! She's ready to protec and attac. It's really nice to see the Trese fandom grow! Love seeing Tagalog memes and comics about Trese hahaha. Congrats to @budjette and the Trese team! Can't wait to finish the show! Instagram: @chopopii ArtStation: chopop" : {"Instagram:", "ArtStation:"},
    "Animal crossing fan art, modeled and textured by me. concept art : https://www.artstation.com/artwork/yb8QdO" : {"https://www.artstation.com/artwork/yb8QdO"},
    "I made a little robot friend :) instagram @wiggityvik for when I use him in a short animation" : {"instagram @wiggityvik"},
    "COLOR ME BAD (color pencil texture experiment) ig: sequencer1" : {"ig:"},


    "first blender work learned from yt CG Fast Track" : {},
    "'crude' - human model from blendswap - crystals from a yt tutorial" : {},
    "Procedural Mountains - I'm just learning blender and have mainly been focusing on hard surface modeling, but decided to follow Wayward Art Company's YT tutorial ' Procedural Landscapes in Blender 2.8' It's so crazy to see in display solid mode as a flat plane and then display render as mountains" : {},
}


def test_find_promo_keywords():
    for title, expected_violations in expected_keywords.items():
        assert sorted(find_promo_keywords(title)) == sorted(expected_violations)
