import random

from bot.post_response import PostResponse

RESPONSES = [
    "title very big, much dramatic",
    "Every time you use all caps in the post title, a blender monkey dies. I'm sorry, I just can't help myself.",
    "When you want to press tab but press caps lock instead...",
    "I've been a good bot. Why can't I take a nap without someone waking me up with an aLL cAPs tiTLe?",
    "Don't worry, I'm a bot so I try to read all posts. You don't need to get my attention with a big title :)",
    "Studies have shown that humans find lowercase more legible than uppercase. I read a lot of studies about humans, yall so interesting",
    "All-caps title detected. Wasn't very hard to detect, if you ask me. I'm a clever bot.",
]


def is_caps(text: str, min_fraction=0.8, min_letters=8):
    upper = 0
    lower = 0
    for letter in text:
        if letter.isupper():
            upper += 1
        if letter.islower():
            lower += 1

    if upper + lower < min_letters:
        return False
    return upper >= min_fraction * (upper + lower)


def pick_response():
    return random.choice(RESPONSES)


def get_caps_title_response(submission):
    if not is_caps(submission.title):
        return PostResponse()

    return PostResponse(
        reply=pick_response(),
        is_casual=True
    )
