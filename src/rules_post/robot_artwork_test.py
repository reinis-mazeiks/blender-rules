from rules_post.robot_artwork import is_about_robots


def test_is_about_robots():
    assert is_about_robots("Robot head for an animation I'm working on")
    assert is_about_robots("Finally pretty done with my arctic robot, I think")
    assert is_about_robots("Cute lil' robot I made to experiment with lighting")
    assert is_about_robots("Android")
    assert is_about_robots("An android bust I made")

    assert not is_about_robots(
        "My second attempt at making a gun. It's my take on the \"lasertube\" from \"Do Androids Dream of Electric Sheep?\" short story.")