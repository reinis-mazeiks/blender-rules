import re

from bot.post_response import PostResponse
from language_util.bot_language import join_keywords

social = "(yt|youtube|channel|handle|twitch|insta|instagram|ig|artstation|socials)"

PROMOTION_KEYWORDS = re.compile(
    f"\\b{social}( \\w+)?(:| @[^\\s]+| account\\b)|"
    # need to exclude youtube from the below, as valid posts can end in "youtube tutorial"
    f"\\b(channel|twitch|insta|instagram) [^\\s]+$|"
    f"\\b(my) (\\w+ ){{0,5}}{social}\\b|"
    "\\b(subscribe|follow me)\\b|"
    "\\b(please) (share|like|follow)\\b|"
    "\\b(\\d+).{0,10}(likes|followers|subscribers)\\b|"
    "(\\b|^)(https?://.+\..+)(\\b|$)",
    flags=re.IGNORECASE
)


def find_promo_keywords(title):
    return {
        violation.group()
        for violation in PROMOTION_KEYWORDS.finditer(title)
    }


def get_excessive_promotion_response(submission):
    keywords = find_promo_keywords(submission.title)

    if len(keywords) == 0:
        return PostResponse()

    return PostResponse(
        report=f"Excessive promo? Title: {join_keywords(keywords)}"
    )
