from rules_post.faq import BUG_MESSAGE, get_faq, RESOURCE_MESSAGE, NEW_USER_MESSAGE


def test_faq():
    assert get_faq("blender randomly crashing ?") == BUG_MESSAGE
    assert get_faq("2.9 crashing immediately on Windows 7, Exception Access Violation error - How to do a workaround?") == BUG_MESSAGE
    assert get_faq("A blender lock-up bug and how to avoid it.") == BUG_MESSAGE
    assert get_faq("Blender keeps crashing") == BUG_MESSAGE
    assert get_faq("blender immediately crashes") == BUG_MESSAGE
    assert get_faq("Blender crashes when rendering. Even the default cube.") == BUG_MESSAGE

    assert get_faq("Where can i find blender textures FREE") == RESOURCE_MESSAGE
    assert get_faq("Free auto retopology/quad remesher addon for blender") == RESOURCE_MESSAGE
    assert get_faq("Could you please suggest any free auto retopo/quad remesher addons for blender.") == RESOURCE_MESSAGE
    assert get_faq("Hi everyone! I'm searching for some good resources ") ==  RESOURCE_MESSAGE
    assert get_faq("Good website for free blueprints") == RESOURCE_MESSAGE

    assert get_faq("New to Blender, I don't know where to start.") == NEW_USER_MESSAGE
    assert get_faq("Hello fellow blenders. Where do i start from??") == NEW_USER_MESSAGE

    assert get_faq("Texture partially goes black in render view (Cycles engine) after applying a normal map.") == None
