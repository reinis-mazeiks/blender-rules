import random
import re

from bot.post_response import PostResponse
from config.blender_flairs import NEED_FEEDBACK, MEME, I_MADE_THIS

ROBOT_KEYWORDS = re.compile(
    "\\b(robots?|robotic|androids?|bots?|automatons?)\\b",
    flags=re.IGNORECASE
)

MAX_LENGTH = 50

RESPONSES = [
    "Blender Rules Bot approves.",
    "I would upvote this if bots were allowed to upvote",
    "Beep boop I like it."
]


def pick_response():
    return random.choice(RESPONSES)


def is_about_robots(title):
    # long titles likely to get false positives
    if len(title) > MAX_LENGTH:
        return False

    has_match = ROBOT_KEYWORDS.search(title)
    return bool(has_match)


ARTWORK_FLAIRS = {
    NEED_FEEDBACK, MEME, I_MADE_THIS
}


def get_robot_artwork_response(submission):
    flair = submission.link_flair_template_id if hasattr(submission, "link_flair_template_id") else None

    if not flair in ARTWORK_FLAIRS:
        return PostResponse()

    if not is_about_robots(submission.title):
        return PostResponse()

    return PostResponse(
        reply=pick_response(),
        is_casual=True
    )
