MAKING_A_GOOD_POST = "https://www.reddit.com/r/blender/wiki/rules_and_guidelines/making_a_good_post"
CONSTRUCTIVE_FEEDBACK = "https://www.reddit.com/r/blender/wiki/rules_and_guidelines/constructive_feedback"

ABOUT_BOT = "https://www.reddit.com/user/blender-rules-bot/comments/ooynk6/i_am_a_friendly_bot/"
BOT_FEEDBACK = "https://www.reddit.com/user/blender-rules-bot/comments/otytg5/blender_rules_bot_feedback/"