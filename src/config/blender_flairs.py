# r/blender flairs
I_MADE_THIS = "c554d4ae-fb0d-11e9-833d-0e683d003f3c"
MEME = "46476ec6-b2f1-11e7-90d2-0e6a7d5167a4"

NEED_HELP = "ed693aa4-0713-11e5-8e82-0e2fc57fa5a7"
ROAST_MY_RENDER = "8824cc42-f7c3-11eb-afe4-3a440481141d"
NEED_FEEDBACK = "e556d66e-0510-11e5-a63c-0e7c4c84e4bf"
NEED_MOTIVATION = "4974145c-f7c4-11eb-ba9d-0226ab88d8e7"
SOLVED_BLENDER = "389614ce-0715-11e5-a1fd-0e2fc57fa5a7"

FREE_TOOLS_AND_ASSETS = "317dc562-fb01-11e9-bc00-0e5b6abe18b6"
TUTORIALS_AND_GUIDES = "cdd465e2-5fc9-11e5-84b1-12162dee14ed"
NEWS_AND_DISCUSSION = "63482e9c-72f7-11e6-a7a1-0ef08722eec9"

ADS_AND_PROMOTIONS = "ff210d52-5078-11e7-bb57-0e978d81da84"

TUTORIAL = "b207e6dc-a47a-11e9-a26b-0e360510a7cc"
DISCUSSION = "2f4b35c2-4f48-11ea-9d93-0e9b441b6233"

# r/blenderhelp flairs
SOLVED_BLENDERHELP = 'c1908c28-fc8c-11e6-9054-0ed8ba489376'
UNSOLVED_BLENDERHELP = 'c81df1d4-fc8c-11e6-bdcd-0e2299faeaee'

# r/blender_rules_bot_tst flairs (for testing)
UNSOLVED_TEST = 'fe22f058-194d-11ec-8364-12adbf17eecd'
SOLVED_TEST = '030a8928-194e-11ec-9e60-a2ffa1162089'

# Common flairs mapped by subreddit
UNSOLVED_MAP = {
    'blender': NEED_HELP,
    'blenderhelp': UNSOLVED_BLENDERHELP,
    'blender_rules_bot_tst': UNSOLVED_TEST,
}

SOLVED_MAP = {
    'blender': SOLVED_BLENDER,
    'blenderhelp': SOLVED_BLENDERHELP,
    'blender_rules_bot_tst': SOLVED_TEST,
}