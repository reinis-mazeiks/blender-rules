import praw

from bot.praw_util_patch import stream_generator
from config.mode import is_dry_run


def reddit_url(uri):
    return f"https://www.reddit.com{uri}"


def get_item_stream(subreddits):
    """
    Given a subreddit, get a stream of all new posts and comments
    :param subreddit:
    :return:
    """

    def submissions_and_comments(**kwargs):
        results = []
        for subreddit in subreddits:
            results.extend(subreddit.new(**kwargs))
            results.extend(subreddit.comments(**kwargs))
        results.sort(key=lambda post: post.created_utc, reverse=True)
        return results

    stream = stream_generator(
        submissions_and_comments,
        skip_existing=not is_dry_run()
    )
    return stream
