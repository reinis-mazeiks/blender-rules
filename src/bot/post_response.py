from typing import Optional, List

import praw

from bot.reddit_api_util import reddit_url
from config.mode import is_dry_run, is_production
from language_util.bot_language import format_post_advice, construct_report


class PostResponse:
    reply: Optional[str]
    report: Optional[str]
    is_casual: bool  # (omit bot signature)
    set_flair: Optional[str]

    def __init__(self, *_, reply=None, report=None, is_casual=False, set_flair=None):
        self.reply = reply
        self.report = report
        self.is_casual = is_casual
        self.set_flair = set_flair

    def is_nothing(self):
        return self.reply is None \
               and self.report is None \
               and self.set_flair is None
    def print_response(self, submission: praw.models.Submission):
        print(f"# {submission.title}\n{submission.selftext}")

        if self.reply is not None:
            print(f"Reply: {self.reply}")

        if self.report is not None:
            print(f"Report: {self.report}")

        if self.set_flair is not None:
            print(f"Set flair ID: {self.set_flair}")

        print("-" * 20)
        print()

    def make_response(self, submission: praw.models.Submission):
        if self.is_nothing():
            return

        print(f"responding to {reddit_url(submission.permalink)}")

        if is_dry_run():
            return self.print_response(submission)
        elif is_production():
            if self.reply is not None:
                needs_signature = isinstance(submission, praw.models.Submission) and not self.is_casual
                message = format_post_advice(self.reply) if needs_signature else self.reply
                submission.reply(message)

            if self.report is not None:
                message = construct_report(self.report)
                submission.report(message)

            if self.set_flair is not None:
                submission.flair.select(self.set_flair)
        else:
            raise Exception("Unexpected mode")


def merge_post_responses(responses: List[PostResponse]):
    replies = [
        response.reply
        for response in responses
        if response.reply is not None
    ]
    reply = "\n\nAlso...\n\n".join(replies) if len(replies) > 0 else None

    reports = [
        response.report
        for response in responses
        if response.report is not None
    ]
    report = "|".join(reports) if len(reports) > 0 else None

    is_casual = all(
        response.is_casual
        for response in responses
        if not response.is_nothing()
    )

    def get_set_flair():
        for response in responses:
            if response.set_flair is not None:
                return response.set_flair
        return None
    set_flair = get_set_flair()


    return PostResponse(reply=reply, report=report, is_casual=is_casual, set_flair=set_flair)


def get_first_actionable_post_response(responses: List[PostResponse]):
    for response in responses:
        if not response.is_nothing():
            return response
    return PostResponse()
