from typing import Optional, List

import praw

from bot.post_response import PostResponse
from bot.reddit_api_util import reddit_url
from config.mode import is_dry_run, is_production
from language_util.bot_language import construct_report, format_post_advice


class CommentResponse:
    reply: Optional[str]
    report: Optional[str]
    is_casual: bool  # (omit bot signature)
    parent_submission_response: Optional[PostResponse]

    def __init__(self, *_, reply=None, report=None, is_casual=False, parent_submission_response=None):
        self.reply = reply
        self.report = report
        self.is_casual = is_casual
        self.parent_submission_response = parent_submission_response

    def is_nothing(self):
        return self.reply is None \
               and self.report is None \
               and (self.parent_submission_response is None or self.parent_submission_response.is_nothing())

    def print_response(self, comment: praw.models.Comment):
        print(comment.body)

        if self.reply is not None:
            print(f"Reply: {self.reply}")

        if self.report is not None:
            print(f"Report: {self.report}")

        if self.parent_submission_response is not None:
            print(f"Parent submission:")
            self.parent_submission_response.print_response(comment.submission)

        print("-" * 20)
        print()

    def make_response(self, comment: praw.models.Comment):
        if self.is_nothing():
            return

        print(f"responding to {reddit_url(comment.permalink)}")

        if is_dry_run():
            return self.print_response(comment)
        elif is_production():
            if self.reply is not None:
                needs_signature = isinstance(comment, praw.models.Submission) and not self.is_casual
                message = format_post_advice(self.reply) if needs_signature else self.reply
                comment.reply(message)

            if self.report is not None:
                message = construct_report(self.report)
                comment.report(message)

            if self.parent_submission_response is not None:
                self.parent_submission_response.make_response(comment.submission)
        else:
            raise Exception("Unexpected mode")


def merge_comment_responses(responses: List[CommentResponse]):
    replies = [
        response.reply
        for response in responses
        if response.reply is not None
    ]
    reply = "\n\nAlso...\n\n".join(replies) if len(replies) > 0 else None

    reports = [
        response.report
        for response in responses
        if response.report is not None
    ]
    report = "|".join(reports) if len(reports) > 0 else None

    is_casual = all(
        response.is_casual
        for response in responses
        if not response.is_nothing()
    )

    return CommentResponse(reply=reply, report=report, is_casual=is_casual)


def get_first_actionable_comment_response(responses: List[CommentResponse]):
    for response in responses:
        if not response.is_nothing():
            return response
    return CommentResponse()
