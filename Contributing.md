# Contributing

Bug fixes are always welcome (e.g. when there is a common false positive) if they come with test cases.

New features can be suggested too, but please check with me (either in an issue or on Reddit) before writing code/making a PR.

I'd like to keep things easy to maintain and helpful, so no, we're not turning this bot into a state-of-the-art NLP AI thing. This bot is dumb simple and that's a feature :)

## Configuration
The bot expects these environment variables:

```.env
USERNAME=blender-rules-bot
PASSWORD=super-secret
CLIENT_ID=super-secret-or-maybe-not-idk
CLIENT_SECRET=super-secret
MODE=DRY_RUN
```

The authentication values you can get from Reddit, and `MODE` needs to be one of `DRY_RUN` or `PRODUCTION`.

## Permissions
For the bot to run successfully, it needs the "Manage Flair" moderator permission in all communities where the "mark_solved" rule is used.

## Structure

Please take a few minutes to familiarize yourself with the structure of this project. I've tried to keep it consistent and simple, so adding new rules should be easy.

The most important packages are `rules_comment` for the comment response rules and `rules_post` for the post rules. These are then used in the `community` package.

## Testing

### Unit Tests
This project uses `pytest` in some places. Not everything is currently unit-tested, just the important and easy-to-test bits.

(more unit tests are welcome – and I've put some effort to ensure that the functions are testable, but mocking the Reddit API objects would probably be a pain anyway)

### Dry Run
By setting `MODE=DRY_RUN`, you can run the bot in dry run mode. In this mode, no real actions will be made. Instead, you will see the replies, reports, etc. in the console output.

### Manual Testing
It's a good idea to manually double-check the bot is fine before releasing it in the wild. Unfortunately there is currently no "clean" mechanism to do this.

You can edit the bot "rule map" in `src/bot/bot.py`:

```python
COMMUNITIES = {
    "blender": Blender(),
    "blenderhelp": BlenderHelp(),
}
```

For example, if you wanted to test the behavior in Blender, comment out the other line and update the subreddit to act in:
```python
COMMUNITIES = {
    "blender_rules_bot_tst": Blender(),
    # "blenderhelp": BlenderHelp(),
}
```
