# Blender Rules!
A friendly reddit bot, always happy to give post advice on r/Blender whenever he can. He's got very basic regex brains, but a kind heart.

[You can find the bot in action here!](https://www.reddit.com/user/blender-rules-bot)

Feedback is always welcome.

Thanks [u/Phoenix-64](https://www.reddit.com/user/Phoenix-64) for the feature automatically marking posts as solved!